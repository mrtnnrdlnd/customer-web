let loadedCars = {};

const initCars = function() {
    getAuthorized("cars").done(function(cars) {
        loadedCars = cars;
        populateTable("#cartable", loadedCars);
    });
}

$(document).on("click", "#orderCarOpenModalBtn", function() {
    $("#orderCarModal .modal-dialog").load("content/cars/orderCarModalContent.html", "", () => {
        let selectedCar = loadedCars.find((car) => {
            return car.id == getSelectedId("#cartable");
        })
    
        let formSelected = $("#orderCarModal div[data-objectid]");
    
        formSelected[0].dataset.objectid = selectedCar.id;
    
        formSelected.html("Bil: <span style='font-weight: bold'>" + selectedCar.name + "</span> <br>"
        + "Storlek: <span style='font-weight: bold'>" + selectedCar.model + "</span><br>"
        + "Pris per dag: <span style='font-weight: bold'>" + selectedCar.pricePerDay + "</span>");
    }); 
});

$(document).on("click", "#orderCarSubmitBtn", function() {
    let newOrder = {
        customer : getCustomer(),
        car : loadedCars.find((car) => {
            return car.id == getSelectedId("#cartable");
        }),
        startDate : $("#startDateInput").val(),
        stopDate : $("#stopDateInput").val()
    }

    if (datesAreValid(newOrder.startDate, newOrder.stopDate)) {
        postAuthorized("ordercar", newOrder).done(() => {
            $("#orderCarModalLabel").parent().addClass("successful-modal");
            $("#orderCarModalLabel").html("Bilen är beställd!");
            $("#startDateInput").prop('disabled', true)
            $("#stopDateInput").prop('disabled', true)
            $("#orderCarSubmitBtn").prop('disabled', true)
        });
    }
});




