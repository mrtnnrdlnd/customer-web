let contentMap = {
    'home': function() {$("#content").load("content/home/home.html")},
    'cars': function() {$("#content").load("content/cars/cars.html", "", initCars())},
    'orders': function() {$("#content").load("content/orders/orders.html", "", initOrders())}
}

let navigateTo = function(page) {
    contentMap[page]();

    $("#pageNavigation a").removeClass("active");
    $("#pageNavigation a[load = '" + page + "'").addClass('active');
}

$(".navbar a[load]").each(function() {
    $(this).on("click", function() {
        navigateTo(this.getAttribute("load"))
    });
})
